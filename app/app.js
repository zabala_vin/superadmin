var dependencies = [
	'ui.router',
	'ui.bootstrap',
	'ui.bootstrap.timepicker',
	'ui.bootstrap.dropdown',

	'main-controller'
]

var app = angular.module('app',dependencies)

.filter('decodeURIComponent', function() {
	
	return window.decodeURIComponent;
})

//defines the routes on our website
.config(function($stateProvider, $urlRouterProvider){
	
	//default route
	$urlRouterProvider.otherwise("main"); 

	$stateProvider
	.state('main', {
		url 			: '/main',		
		views			: {
			'topbar' 			: { templateUrl : 'app/components/nav/nav.html',
									controller  : '' },
			'sidebar'          	: { templateUrl : '',
									controller  : '' },
			'content'			: { templateUrl : 'app/components/main/main.html',
									controller 	: 'mainCtrl' }
		}
	})
})